const projetos = [
    {
        id: "0",
        tipo: "brand",
        titulo: "karioka | design de logo",
        descricao: "Estamparia com temática voltada para a cultura pop do Rio de Janeiro. Foi minha marca por mais de três anos, até o projeto da estamparia ser iniciado.",
        imagemSrc: "/img/portfolio/001/image01.png",
        tituloDois: "construção",
        descricaoDois: "Ícone baseado em círculos, formando a silhueta do Pão de Açúcar em 'negative space' sobre o círculo azul, que representa o céu.",
        imagemSrcDois: "/img/portfolio/001/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "Letras desenhadas a partir de linhas e círculos.",
        imagemSrcTres: "/img/portfolio/001/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "Pantone 15-3919 TCX - Serenity (Cor do Ano de 2016)",
        imagemSrcQuatro: "/img/portfolio/001/image04.png",
        link: ""
    },
    {
        id: "1",
        tipo: "brand",
        titulo: "horus | design de logo",
        descricao: "Consultoria em energia, eletricidade e serviços associados.",
        imagemSrc: "/img/portfolio/002/image01.png",
        tituloDois: "construção",
        descricaoDois: "Ícone baseado em círculos, formando um olho e o bico de um falcão, imagem associada ao deus egípcio Horus.",
        imagemSrcDois: "/img/portfolio/002/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "Letras desenhadas a partir de linhas e círculos.",
        imagemSrcTres: "/img/portfolio/002/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "Pantone 18-3838 TCX - Ultra Violet (Cor do Ano de 2018)",
        imagemSrcQuatro: "/img/portfolio/002/image04.png",
        link: ""
    },
    {
        id: "2",
        tipo: "brand",
        titulo: "revoar | naming e design de logo",
        descricao: "Agência de turismo e eventos. O nome é uma analogia ao francês 'revoir', além de significar 'voar de novo' (re-voar).",
        imagemSrc: "/img/portfolio/003/image01.png",
        tituloDois: "construção",
        descricaoDois: "Ícone baseado em círculos, formando a silhueta da Torre Eiffel, com dois pássaros em sobreposição aos andares da torre, como representação do nome.",
        imagemSrcDois: "/img/portfolio/003/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "Gruppo Regular",
        imagemSrcTres: "/img/portfolio/003/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "Pantone 7557 C<br>Pantone 1807 C<br>Pantone 2727 C",
        imagemSrcQuatro: "/img/portfolio/003/image04.png",
        link: ""
    },
    {
        id: "3",
        tipo: "brand",
        titulo: "ele veste bem | design de logo",
        descricao: "Blog de moda.",
        imagemSrc: "/img/portfolio/004/image01.png",
        tituloDois: "construção",
        descricaoDois: "Listras verticais sobrepostas as silhuetas de uma barba e uma gravata borboleta, produtos de desenho digital/vetorial.",
        imagemSrcDois: "/img/portfolio/004/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "Josefin Sans Light",
        imagemSrcTres: "/img/portfolio/004/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "Pantone Black 7 C<br>Pantone 7671 C",
        imagemSrcQuatro: "/img/portfolio/004/image04.png",
        link: ""
    },
    {
        id: "4",
        tipo: "brand",
        titulo: "greencoast | design de logo",
        descricao: "Serviços em eletricidade, energia, projetos e manutenção.",
        imagemSrc: "/img/portfolio/005/image01.png",
        tituloDois: "construção",
        descricaoDois: "Folha de árvore com nervura em formato de um transistor NPN, representando o ramo de atuação da empresas (energia, eletrônica e eficiência), aplicada em negativo a um círculo, produto de desenho digital/vetorial.",
        imagemSrcDois: "/img/portfolio/005/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "Exo 2 Light<br>Exo 2 Thin",
        imagemSrcTres: "/img/portfolio/005/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "Pantone 7489 C",
        imagemSrcQuatro: "/img/portfolio/005/image04.png",
        link: ""
    },
    {
        id: "5",
        tipo: "brand",
        titulo: "carregado de axé | design de logo",
        descricao: "Loja virtual de artigos culturais e religiosos (projeto em andamento).",
        imagemSrc: "/img/portfolio/006/image01.png",
        tituloDois: "construção",
        descricaoDois: "Ícone baseado em círculos. As cores foram inspiradas na bandeira do Estado de Osún, na Nigéria.",
        imagemSrcDois: "/img/portfolio/006/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "Futurafrica Regular",
        imagemSrcTres: "/img/portfolio/006/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "Pantone 7533 C<br>Pantone 7568 C<br>Pantone 7427 C<br>Pantone 7743 C<br>Pantone 7563 C",
        imagemSrcQuatro: "/img/portfolio/006/image04.png",
        link: ""
    },
    {
        id: "6",
        tipo: "brand",
        titulo: "convivência | design/vetorização de logo",
        descricao: "Atêlie/projeto cultural formado por artistas portadores da Síndrome de Down.",
        imagemSrc: "/img/portfolio/007/image01.png",
        tituloDois: "construção",
        descricaoDois: "Vetorização e colorização de arte feita por uma das integrantes do projeto.",
        imagemSrcDois: "/img/portfolio/007/image02.png",
        tituloTres: "tipografia",
        descricaoTres: "NovaMono Regular",
        imagemSrcTres: "/img/portfolio/007/image03.png",
        tituloQuatro: "paleta de cores",
        descricaoQuatro: "#323F3C<br>#40534F<br>#557162<br>#698F70<br>#7FAF7F",
        imagemSrcQuatro: "/img/portfolio/007/image04.png",
        link: ""
    },
    {
        id: "7",
        tipo: "destaque",
        titulo: "moovit | mapa de transporte metropolitano do RJ (2017)",
        descricao: "Projeto desenvolvido em parceira para com a Moovit que disponibilizou, em seu aplicativo oficial, um mapa das linhas de transporte em massa da região metropolitana do Rio.",
        imagemSrc: "/img/portfolio/008/image01.png",
        tituloDois: "rotas na palma da mão",
        descricaoDois: "Disponível no app oficial da Moovit, o mapa visava facilitar a vida dos cariocas, dando uma visão detalhada das principais linhas de trem, metrô, brt, etc.",
        imagemSrcDois: "/img/portfolio/008/image02.png",
        tituloTres: "na mídia",
        descricaoTres: "O Jornal O Dia, um dos maiores da região, replicou uma matéria do Diário do Transporte, noticiando a disponibilização desse recurso em Maio de 2017, destacando a parceria entre a Moovit e a Karioka (minha marca, na época).",
        imagemSrcTres: "/img/portfolio/008/image03.png",
        tituloQuatro: "do relevante ao essencial",
        descricaoQuatro: "O mapa destacava, inclusive, as estações que tinham recursos de acessibilidade, visando o conforto e bem estar dos usuários. O mapa ficou disponível durante mais de um ano, até sofrer alterações e ser substituído pelo mapa oficial da Prefeitura do Rio.",
        imagemSrcQuatro: "/img/portfolio/008/image04.png",
        link: "<a href='/files/001.pdf' class='leftlink' target='_blank'>Veja o mapa (2017)</a>"
    },
    {
        id: "8",
        tipo: "code",
        titulo: "ecoleta | #nextlevelweek 1.0",
        descricao: "Criado na trilha Starter da primeira #NextLevelWeek da Rocketseat, o Ecoleta é um marketplace de coleta de resíduos. Para esse projeto, utilizamos JavaScript, CSS, HTML, Node.js, Express, Nunjucks e SQLite.",
        imagemSrc: "/img/portfolio/009/image01.png",
        tituloDois: "busca por localidade",
        descricaoDois: "Os pontos cadastrados podem ser encontrados através do link da página principal que redireciona a um modal de busca.",
        imagemSrcDois: "/img/portfolio/009/image02.png",
        tituloTres: "resultados de pesquisa",
        descricaoTres: "A busca retorna em uma página dinâmica, preenchida com os dados do banco de dados (SQLite), através da template engine Nunjucks.",
        imagemSrcTres: "/img/portfolio/009/image03.png",
        tituloQuatro: "cadastro dos pontos de coleta",
        descricaoQuatro: "Os pontos são cadastrados através de formulário que utilizou, inclusive, uma API dos Correios para preenchimento das cidades correspondentes ao estado selecionado. Nessa minha versão, permito a inclusão de novos pontos para teste da plataforma e o servidor Node.js apaga os novos registros, dois minutos após a inclusão.",
        imagemSrcQuatro: "/img/portfolio/009/image04.png",
        link: "<a href='https://euwg-nlw1-ecoleta.glitch.me/' class='leftlink' target='_blank'>Conheça o site</a><a href='https://github.com/euwilliamgoncalves/nlw1_ecoleta' class='leftlink' target='_blank'>Veja o repositório</a>"
    },
    {
        id: "9",
        tipo: "code",
        titulo: "bootstrap | digital innovation one",
        descricao: "Essa página foi criada para exercitar os conhecimentos adquiridos no curso de Bootstrap da Digital Innovation One, ministrado pelo professor Rafael Galleani.",
        imagemSrc: "/img/portfolio/010/image01.png",
        tituloDois: "objetivo",
        descricaoDois: "Nesse curso, foram abordados os principais recursos desse framework, além de sua aplicação prática criando um site.",
        imagemSrcDois: "/img/portfolio/010/image02.png",
        tituloTres: "meu toque",
        descricaoTres: "A partir disso, criei essa página, buscando apoio na documentação oficial do Bootstrap, para alguns pequenos ajustes, e acrescentando duas fontes do repositório da Google, para customizar a aparência da página.",
        imagemSrcTres: "/img/portfolio/010/image03.png",
        tituloQuatro: "sobre o bootstrap",
        descricaoQuatro: "É um framework para desenvolvimento front-end de páginas web, focado na responsividade do projeto. Utilizando grids, a página pode se adaptar a qualquer tela, ficando mais legível e adequada a cada dispositivo. É um dos recursos mais utilizados para desenvolvimento web, devido a sua flexibilidade e as inúmeras possibilidades de aplicação.",
        imagemSrcQuatro: "/img/portfolio/010/image04.png",
        link: "<a href='https://euwilliamgoncalves.github.io/dio_bootstrap/' class='leftlink' target='_blank'>Conheça o site</a><a href='https://github.com/euwilliamgoncalves/dio_bootstrap' class='leftlink' target='_blank'>Veja o repositório</a>"
    },
    {
        id: "10",
        tipo: "destaque",
        titulo: "knowleed | projeto mega hack 3.0",
        descricao: "Projeto criado durante o Hackathon Mega Hack 3.0 da Shawee, atendendo o desafio da Árvore Educação e alcançando o Top 20 nele.",
        imagemSrc: "/img/portfolio/011/image01.png",
        tituloDois: "a solução",
        descricaoDois: "Uma plataforma de leitura gamificada com sistemas de pontuação, nível, conquistas e ranking, premiando os alunos mais assíduos na plataforma com recompensas no mundo real. Para construir a aplicação, utilizamos HTML, CSS, JavaScript, Node.js, Express e Nunjucks.",
        imagemSrcDois: "/img/portfolio/011/image02.png",
        tituloTres: "o aprendizado",
        descricaoTres: "É fundamental que você tenha um destino para tudo o que for codar. Mesmo que seja pelo aprendizado, é super importante dar um norte ao produto. Se não há um comprador definido, para sua aplicação, dificilmente você vende.<br><br>E nós alcançamos nosso objetivo nesse desafio, com um grande trabalho de pesquisa, ideação, definição de personas, estudo de mercado e entendendo o comportamento de potenciais usuários.",
        imagemSrcTres: "/img/portfolio/011/image03.png",
        tituloQuatro: "parceiros de equipe",
        descricaoQuatro: "<a href='https://www.linkedin.com/in/nayra-cruz/' class='leftlink' target='_blank'>Nayra Cruz - UX</a><a href='https://www.linkedin.com/in/danielmeiato/' class='leftlink' target='_blank'>Daniel Meiato - PM</a><a href='https://www.linkedin.com/in/bernardojachegou/' class='leftlink' target='_blank'>Michel Bernardo - Dev</a><a href='https://www.linkedin.com/in/thiagopederzollimdasilva/' class='leftlink' target='_blank'>Thiago Pederzolli - Dev</a>",
        imagemSrcQuatro: "/img/portfolio/011/image04.jpg",
        link: "<a href='https://www.youtube.com/watch?v=cAm7oerJ_sY' class='leftlink' target='_blank'>Vídeo Demo</a><a href='https://www.youtube.com/watch?v=RVqm_sbBxNI' class='leftlink' target='_blank'>Vídeo do Pitch</a><a href='https://github.com/bernardojachegou/projeto_knowleed' class='leftlink' target='_blank'>Repositório</a>"
    },
]

var populateSelect = () => {
    const projetoSelect = document.querySelector("#projetos-lista");
    projetoSelect.innerHTML += `<option value="" disabled selected>Selecione um projeto</option>`
    projetoSelect.innerHTML += `<optgroup label="PROJETOS EM DESTAQUE">`
    for (var projeto in projetos) {
        if (projetos[projeto].tipo == "destaque") {
            projetoSelect.innerHTML += `<option value="${projetos[projeto].id}">${projetos[projeto].titulo}</option>`
        }
    }
    projetoSelect.innerHTML += `</optgroup>`
    projetoSelect.innerHTML += `<optgroup label="DEV: SITES, GAMES, ETC">`
    for (var projeto in projetos) {
        if (projetos[projeto].tipo == "code") {
            projetoSelect.innerHTML += `<option value="${projetos[projeto].id}">${projetos[projeto].titulo}</option>`
        }
    }
    projetoSelect.innerHTML += `</optgroup>`
    projetoSelect.innerHTML += `<optgroup label="DESIGN">`
    for (var projeto in projetos) {
        if (projetos[projeto].tipo == "brand") {
            projetoSelect.innerHTML += `<option value="${projetos[projeto].id}">${projetos[projeto].titulo}</option>`
        }
    }
    projetoSelect.innerHTML += `</optgroup>`
}

populateSelect();

const corpoPortfolio = document.querySelector("#conteudo-portfolio");

var projetoValor = 10;

function sumir() {
    corpoPortfolio.classList.remove('visivel');
    corpoPortfolio.classList.add('invisivel');
}

function aparecer() {
    corpoPortfolio.classList.remove('invisivel');
    corpoPortfolio.classList.add('visivel');
}

function projetoEscolhido(event) {
    projetoValor = event.target.value;
    sumir();
    preencherPortfolio();
    setTimeout(aparecer, 1000);
}

document.querySelector("#projetos-lista").addEventListener("change", projetoEscolhido);

function preencherPortfolio() {
    corpoPortfolio.innerHTML = `
    <div class="description">
        <h2>${projetos[projetoValor].titulo}</h2>
        <p>${projetos[projetoValor].descricao}</p>
        ${projetos[projetoValor].link}
    </div>
    <div class="image"><img src="${projetos[projetoValor].imagemSrc}" alt=""></div>
    <div class="description">
        <h2>${projetos[projetoValor].tituloDois}</h2>
        <p>${projetos[projetoValor].descricaoDois}</p>
    </div>
    <div class="image"><img src="${projetos[projetoValor].imagemSrcDois}" alt=""></div>
    <div class="description">
        <h2>${projetos[projetoValor].tituloTres}</h2>
        <p>${projetos[projetoValor].descricaoTres}</p>
    </div>
    <div class="image"><img src="${projetos[projetoValor].imagemSrcTres}" alt=""></div>
    <div class="description">
        <h2>${projetos[projetoValor].tituloQuatro}</h2>
        <p>${projetos[projetoValor].descricaoQuatro}</p>
    </div>
    <div class="image"><img src="${projetos[projetoValor].imagemSrcQuatro}" alt=""></div>
    `;
}

preencherPortfolio();

const stacks = [
    {name: "JavaScript", img: "img/stacks/javascript.png"},
    {name: "CSS", img: "img/stacks/css.png"},
    {name: "HTML", img: "img/stacks/html.png"},
    {name: "Node.js", img: "img/stacks/nodejs.png"},
    {name: "Python", img: "img/stacks/python.png"},
    {name: "ReactJs", img: "img/stacks/reactjs.png"},
    {name: "GitHub", img: "img/stacks/github.png"},
    {name: "Git", img: "img/stacks/git.png"},
    {name: "Illustrator", img: "img/stacks/illustrator.svg"},
    {name: "Photoshop", img: "img/stacks/photoshop.png"}
];

var stackList = document.querySelector('#stacks-list');
var stackTemplate=[];

var stackWrite = () => {
    for (var stack in stacks) {
        stackTemplate[stack] = '<div class="stack"><img src="'+stacks[stack].img+'" alt="'+stacks[stack].name+'"><p>'+stacks[stack].name+'</p></div>';
    }
};

stackWrite();

stackList.innerHTML = stackTemplate.join(" ");

const courses = [
    {name: "Desenvolvimento Node.js", school: "Digital Innovation One", link: "https://web.digitalinnovation.one/track/desenvolvimento-nodejs", badge: "icofont-license", date: "Setembro / 2020", finished: false},
    {name: "Web Design Completo", school: "Origamid", link: "https://www.origamid.com/curso/web-design-completo", badge: "icofont-badge", date: "Agosto / 2020", finished: false},
    {name: "Cloud Computing & Serverless", school: "Digital Innovation One + Microsoft", link: "https://drive.google.com/file/d/1S-12a5AHkttkwOeuyWacGU9yJBTyj7TY/view?usp=sharing", badge: "icofont-license", date: "Julho / 2020", finished: true},
    {name: "Hackathon Mega Hack 3.0", school: "Shawee", link: "https://drive.google.com/file/d/1IhujkTXeJl2iNoYNDRvJB7urFh7kl-Or/view?usp=drivesdk", badge: "icofont-star", date: "Julho / 2020", finished: true},
    {name: "#ImersãoGameDev", school: "Alura", link: "https://drive.google.com/file/d/12_adv5Dd3wteVbE8lvf1j6jOjEkqFjnK/view?usp=sharing", badge: "icofont-star", date: "Junho / 2020", finished: true},
    {name: "Desenvolvedor Front-End ReactJs", school: "Digital Innovation One", link: "https://drive.google.com/file/d/1X48G2DzlqjwTsD0rNFqTSBUkDa5kVxIg/view?usp=sharing", badge: "icofont-license", date: "Junho / 2020", finished: true},
    {name: "Soft Skills e Hard Skills", school: "Solides", link: "https://drive.google.com/file/d/1CL3cQDAilC9hm2ftb-T3wWYAPwPiR2_D/view?usp=sharing", badge: "icofont-badge", date: "Maio / 2020", finished: true},
    {name: "NextLevelWeek 1.0", school: "RocketSeat", link: "https://euwg-nlw1-ecoleta.glitch.me/", badge: "icofont-star", date: "Maio / 2020", finished: true},
    {name: "Master Python Fundamentals the Fun Way", school: "BitDegree", link: "https://drive.google.com/file/d/1B4YrIPdOG0v9BAb66sPN_vS62HZzNVF7/view?usp=sharing", badge: "icofont-badge", date: "Maio / 2020", finished: true},
    {name: "Inteligência Emocional", school: "Escola Conquer", link: "https://drive.google.com/file/d/1O7j-A87B1brr5gsuHeV9Ua_h0f8d4wu-/view?usp=sharing", badge: "icofont-badge", date: "Maio / 2020", finished: true},
    {name: "Introduction to IoT", school: "Cisco Networking Academy", link: "https://drive.google.com/file/d/1Pxh6nj29GUj4kxkNAxuz6IbOsfKqdSdt/view?usp=sharing", badge: "icofont-badge", date: "Maio / 2020", finished: true},
    {name: "Mentalidade de Desenvolvimento Contínuo", school: "PUCRS", link: "https://drive.google.com/file/d/1fnuytJQZPEO8IX8gOG86KNy9ujGu3ZkM/view?usp=sharing", badge: "icofont-badge", date: "Outubro / 2019", finished: true},
    {name: "Produtividade, Gestão do Tempo e Propósito", school: "PUCRS", link: "https://drive.google.com/file/d/1QJauFeHBp796R23efFtPPxSCWqID95AT/view?usp=sharing", badge: "icofont-badge", date: "Agosto / 2019", finished: true}
];

var certificateList = document.querySelector('#certificate-list');
var courseList = document.querySelector('#course-list');

certificateTemplate = [];
courseTemplate = [];

var courseWrite = () => {
    for (var course in courses) {
        if (courses[course].finished == true) {
            certificateTemplate[course] = '<div class="card"><a target="_blank" href="'+courses[course].link+'"><i class="'+courses[course].badge+'"></i><span><h2>'+courses[course].name+'<br><i>'+courses[course].school+'</i></h2><p>'+courses[course].date+'</p></span></a></div>';
        }else{
            courseTemplate[course] = '<div class="card"><a target="_blank" href="'+courses[course].link+'"><i class="'+courses[course].badge+'"></i><span><h2>'+courses[course].name+'<br><i>'+courses[course].school+'</i></h2><p>'+courses[course].date+'</p></span></a></div>';
        }
    }
};

courseWrite();

certificateList.innerHTML = certificateTemplate.join(" ");
courseList.innerHTML = courseTemplate.join(" ");